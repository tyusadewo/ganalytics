import React, { Component } from "react";
import { Menu, Button, Icon } from "antd";

const { SubMenu } = Menu;

// const Submenu = Menu.SubMenu;
// const MenuItemGroup = Menu.ItemGroup;

class LeftMenuHeader extends Component {
  render() {
    return (
      <Menu
        mode="horizontal"
        theme="dark"
        defaultSelectedKeys={["0"]}
        style={{ lineHeight: "64px" }}
      >
        <Menu.Item key="1" style={{ padding: "0px 10px" }}>
          <Button
            type="primary"
            className="header-button button-selected"
            // class="button-selected"
          >
            Home
          </Button>
        </Menu.Item>
        <Menu.Item key="2" style={{ padding: "0 10px" }}>
          <Button type="primary" className="header-button">
            Features
          </Button>
        </Menu.Item>
        <Menu.Item key="3" style={{ padding: "0 10px" }}>
          <Button type="primary" className="header-button">
            Resources
          </Button>
        </Menu.Item>
        <Menu.Item key="4" style={{ padding: "0 10px" }}>
          <Button type="primary" className="header-button">
            Docs
          </Button>
        </Menu.Item>
        <SubMenu
          title={
            <span className="submenu-title-wrapper">
              <Button type="primary" className="header-button">
                More
                <Icon type="down" padding="2px" />
              </Button>
            </span>
          }
        >
          <Menu.Item key="Blog" style={{ color: "white" }}>
            Blog
          </Menu.Item>
          <Menu.Item key="About" style={{ color: "white" }}>
            About
          </Menu.Item>
          <Menu.Item key="Pricing" style={{ color: "white" }}>
            Pricing
          </Menu.Item>
          <Menu.Item key="Jobs" style={{ color: "white" }}>
            Jobs
          </Menu.Item>
          <Menu.Item key="Customer" style={{ color: "white" }}>
            Customer
          </Menu.Item>
          {/* <Menu.Item key="setting:1">Option 1</Menu.Item> */}
          {/* <Menu.Item key="setting:2">Option 2</Menu.Item> */}

          {/* <Menu.ItemGroup title="About"> */}
          {/* <Menu.Item key="setting:3">Option 3</Menu.Item> */}
          {/* <Menu.Item key="setting:4">Option 4</Menu.Item> */}
          {/* </Menu.ItemGroup> */}
        </SubMenu>
        {/* <Menu.Item key="5" style={{ padding: "0 10px" }}>
          <Button type="primary" className="header-button">
            More
          </Button>
        </Menu.Item> */}
      </Menu>
    );
  }
}

export default LeftMenuHeader;
