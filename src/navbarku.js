import React, { Component } from "react";
import { Layout, Breadcrumb, Row, Col, Menu, Icon, Button } from "antd";
import logo from "./logo.svg";
import LeftMenuHeader from "./LeftMenuHeader";
import RightMenuHeader from "./RightMenuHeader";

import "antd/dist/antd.css";
// import "./custom.css";
import "./custom.css";
const { Header, Footer, Sider, Content } = Layout;

const Submenu = Menu.submenu;
const MenuItemGroup = Menu.ItemGroup;

class Navbarku extends Component {
  render() {
    return (
      <Layout>
        <Header>
          <div>
            <Row style={{ verticalAlign: "middle" }}>
              <Col span={6}>
                <Row
                  type="flex"
                  justify="start"
                  style={{ alignItems: "center" }}
                >
                  <Col span={6}>
                    <img
                      src={logo}
                      className="App-logo"
                      alt="logo"
                      width="70"
                      height="70"
                    />
                  </Col>
                  <Col span={6}>
                    <span class="header-logo-text" style={{ color: "white" }}>
                      TYUSADEWO
                    </span>
                  </Col>
                </Row>
              </Col>
              <Col span={12} className="MenuHeader">
                {/* <div className="MenuHeader"> */}
                <LeftMenuHeader />
                {/* </div> */}
              </Col>
              <Col span={6} className="MenuHeader">
                <RightMenuHeader />
              </Col>
              {/* <Col span={6}></Col> */}
            </Row>
          </div>
        </Header>
        <Content style={{ padding: "0 50px" }}>
          <Breadcrumb style={{ margin: "16px 0" }}>
            <Breadcrumb.Item>Home</Breadcrumb.Item>
            <Breadcrumb.Item>List</Breadcrumb.Item>
            <Breadcrumb.Item>App</Breadcrumb.Item>
          </Breadcrumb>
          <div style={{ background: "#fff", padding: 24, minHeight: 280 }}>
            Content
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Ant Design ©2019 Created by Tyusadewo
        </Footer>
      </Layout>
    );
  }
}
export default Navbarku;
