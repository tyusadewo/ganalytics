import React, { Component } from "react";
import { Layout, Button, Menu } from "antd";

class RightMenuHeader extends Component {
  render() {
    return (
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={["0"]}
        style={{ lineHeight: "64px" }}
      >
        <Menu.Item key="6">
          <Button type="primary" className="login-button">
            Login
          </Button>
        </Menu.Item>
        <Menu.Item key="7">
          <Button type="primary">Sign Up</Button>
        </Menu.Item>
      </Menu>
    );
  }
}

export default RightMenuHeader;
